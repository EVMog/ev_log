﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EV_Log_Proto01_Trace.Helpers;
using System.Text.RegularExpressions;

namespace EV_Log_Proto01_Trace.Model
{
    public static class Filter
    {
        public static string _version { get; set; }

        public static bool WhiteFilter(List<string> filterValues, TraceEvent data = null, string testData = "")
        {            
            var message = data != null ? data.FormattedMessage.ToUpper() : testData.ToUpper();
            
            for (var i = 0; i < filterValues.Count; i++)
            {
                if (!message.Contains(filterValues[i].ToUpper()))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool PageFilter(TraceEvent data = null, string testData = "")
        {
            List<string> filterValues = null;

            if (_version == "14.037587.0")
            {
                filterValues = FilterLists.pageFilter01;
            }

            return data != null ? WhiteFilter(data: data, filterValues: filterValues) : WhiteFilter(testData: testData, filterValues: filterValues);
        }

        public static bool ReportFilter(TraceEvent data = null, string testData = "")
        {
            List<string> filterValues = null;

            if (_version == "14.037587.0")
            {
                filterValues = FilterLists.reportClose;
            }

            return data != null ? WhiteFilter(data: data, filterValues: filterValues) : WhiteFilter(testData: testData, filterValues: filterValues);
        }


        public static bool CompanyFilter(TraceEvent data = null, string testData = "")
        {
            List<string> filterValues = null;

            if (_version == "14.037587.0")
            {
                filterValues = FilterLists.companyOpen;
            }

            return data != null ? WhiteFilter(data: data, filterValues: filterValues) : WhiteFilter(testData: testData, filterValues: filterValues);
        }

        public static bool UserFilter(TraceEvent data = null, string testData = "")
        {
            List<string> filterValues = null;

            if (_version == "14.037587.0")
            {
                filterValues = FilterLists.user;
            }

            return data != null ? WhiteFilter(data: data, filterValues: filterValues) : WhiteFilter(testData: testData, filterValues: filterValues);
        }

        public static string GetCompanyName(TraceEvent data = null, string testData = "")
        {
            return data != null ? GetPayload(data.FormattedMessage.ToUpper(), EventPayloads.CompanyStart, EventPayloads.CompanyEnd) : GetPayload(testData.ToUpper(), EventPayloads.CompanyStart, EventPayloads.CompanyEnd);
        }

        public static string GetPageId(TraceEvent data = null, string testData = "")
        {
            return data != null ? GetPayload(data.FormattedMessage.ToUpper(), EventPayloads.PageStart, EventPayloads.PageEnd) : GetPayload(testData.ToUpper(), EventPayloads.PageStart, EventPayloads.PageEnd);
        }

        public static string GetReportId(TraceEvent data = null, string testData = "")
        {
            var payload = data != null ? GetPayload(data.FormattedMessage.ToUpper(), EventPayloads.ReportStart, EventPayloads.ReportEnd) : GetPayload(testData.ToUpper(), EventPayloads.ReportStart, EventPayloads.ReportEnd);
            return payload.Trim('"', '.');
        }

        public static string GetExecutionTime(TraceEvent data = null, string testData = "")
        {
            return data != null ? GetPayload(data.FormattedMessage.ToUpper(), EventPayloads.ExecutionTimeStart, EventPayloads.ExecutionTimeEnd) : GetPayload(testData.ToUpper(), EventPayloads.ExecutionTimeStart, EventPayloads.ExecutionTimeEnd);
        }

        public static string GetUserInfo(TraceEvent data = null, string testData = "")
        {
            return data != null ? GetPayload(data.FormattedMessage.ToUpper(), EventPayloads.UserInfoStart, EventPayloads.UserInfoEnd) : GetPayload(testData.ToUpper(), EventPayloads.UserInfoStart, EventPayloads.UserInfoEnd);
        }

        public static string GetUserName(TraceEvent data = null, string testData = "")
        {
            return data != null ? Regex.Match(GetUserInfo(data), "([^\\\\]+$)").Value : Regex.Match(GetUserInfo(testData: testData), "([^\\\\]+$)").Value;
        }

        public static string GetNSTName(TraceEvent data = null, string testData = "")
        {
            return data != null ? Regex.Match(GetUserInfo(data), ".*(?=\\\\)").Value : Regex.Match(GetUserInfo(testData: testData), ".*(?=\\\\)").Value;
        }

        public static string GetPayload(string data, string start, string end)
        {
            start = start.ToUpper();
            end = end.ToUpper();

            char[] trims = { '\n', '\r', ' ' };
            var Start = data.IndexOf(start);
            Start = Start + start.Length;

            var Length = data.IndexOf(end) - Start;

            return data.Substring(Start, Length).Trim(trims);
        }
    }
}

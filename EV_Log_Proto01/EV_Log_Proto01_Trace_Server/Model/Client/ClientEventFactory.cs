﻿using EV_Log_Proto01_Trace.Helpers;
using Microsoft.Diagnostics.Tracing;
using System;

namespace EV_Log_Proto01_Trace.Model.Client
{
    public static class ClientEventFactory
    {
        public static ClientEventBase Create(TraceEvent data = null, string testData = null)
        {
            if (data != null)
            {
                if (Filter.PageFilter(data))
                {
                    var pageId = Filter.GetPageId(data);

                    if (pageId != "0")
                    {
                        return new ClientPageEvent(
                            data.TimeStamp,
                            DataTypes.Page,
                            Tracer._companyId,
                            Tracer._userName,
                            Tracer._NST,
                            pageId
                        );
                    }

                    return null;
                }
                else if (Filter.ReportFilter(data))
                {
                    return new ClientReportEvent(
                        data.TimeStamp,
                        DataTypes.Report,
                        Tracer._companyId,
                        Tracer._userName,
                        Tracer._NST,
                        Filter.GetReportId(data),
                        Filter.GetExecutionTime(data),
                        CalcStartTime(data.TimeStamp, Filter.GetExecutionTime(data))
                        );
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if (Filter.PageFilter(testData: testData))
                {
                    var pageId = Filter.GetPageId(testData: testData);
                    var timestamp = DateTime.Now;

                    if (pageId != "0")
                    {
                        return new ClientPageEvent(
                            timestamp,
                            DataTypes.Page,
                            Tracer._companyId,
                            Tracer._userName,
                            Tracer._NST,
                            pageId
                        );
                    }

                    return null;
                }
                else if (Filter.ReportFilter(testData: testData))
                {
                    var timestamp = DateTime.Now;

                    return new ClientReportEvent(
                        timestamp,
                        DataTypes.Report,
                        Tracer._companyId,
                        Tracer._userName,
                        Tracer._NST,
                        Filter.GetReportId(testData: testData),
                        Filter.GetExecutionTime(testData: testData),
                        CalcStartTime(timestamp, Filter.GetExecutionTime(testData: testData))
                        );
                }
                else
                {
                    return null;
                }
            }
        }

        private static DateTime CalcStartTime(DateTime end, string executionTime)
        {
            return end.AddMilliseconds(0 - Convert.ToDouble(executionTime));
        }
    }
}

﻿using System;

namespace EV_Log_Proto01_Trace.Model.Client
{
    public abstract class ClientEventBase
    {
        public ClientEventBase(DateTime timestamp, string type, string companyId, string userId, string nst)
        {
            Timestamp = timestamp;
            Type = type;
            CompanyID = companyId;
            UserID = userId;
            NST = nst;
        }

        public DateTime Timestamp { get; set; }
        public string Type { get; set; }
        public string CompanyID { get; set; }
        public string UserID { get; set; }
        public string NST { get; set; }
    }
}

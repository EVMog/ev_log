﻿using System;

namespace EV_Log_Proto01_Trace.Model.Client
{
    public class ClientPageEvent : ClientEventBase
    {
        public ClientPageEvent(DateTime timestamp, string type, string companyId, string userId, string nst, string pageId)
        : base(timestamp, type, companyId, userId, nst)
        {
            PageID = pageId;
        }

        public string PageID { get; set; }
    }
}

﻿using System;

namespace EV_Log_Proto01_Trace.Model.Client
{
    public class ClientReportEvent : ClientEventBase
    {
        public ClientReportEvent(DateTime timestamp, string type, string companyId, string userId, string nst, string reportId, string executionTime, DateTime eventStart)
        : base(timestamp, type, companyId, userId, nst)
        {
            ReportID = reportId;
            ExecutionTime = executionTime;
            EventStart = eventStart;
        }

        public string ReportID { get; set; }
        public string ExecutionTime { get; set; }
        public DateTime EventStart { get; set; }
    }
}

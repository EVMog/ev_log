﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Model.Server
{
    public class ServerUserEvent : ServerEventBase
    {
        public ServerUserEvent(string key, string sessionId, string userId)
            : base(key)
        {
            SessionID = sessionId;
            UserID = userId;
        }
        public string SessionID { get; set; }
        public string UserID { get; set; }
    }
}

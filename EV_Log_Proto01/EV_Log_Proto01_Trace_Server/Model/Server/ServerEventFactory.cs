﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Model.Server
{
    public static class ServerEventFactory
    {
        public static ServerEventBase Create(TraceEvent data)
        {
            return new ServerUserEvent(
                Filter.GetUserName(data),
                Filter.GetUserName(data),
                "Michael O"
            );
        }
    }
}

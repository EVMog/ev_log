﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Model.Server
{
    public class ServerEventBase
    {
        public ServerEventBase(string key)
        {
            Key = key;
        }

        public string Key { get; set; }
    }
}

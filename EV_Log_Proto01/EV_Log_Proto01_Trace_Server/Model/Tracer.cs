﻿using Microsoft.Diagnostics.Tracing;
using Microsoft.Diagnostics.Tracing.Parsers;
using Microsoft.Diagnostics.Tracing.Session;
using EV_Log_Proto01_Trace.Storages;
using System;
using EV_Log_Proto01_Trace.Model.Client;
using EV_Log_Proto01_Trace.Model.Server;
using System.Configuration;

namespace EV_Log_Proto01_Trace.Model
{
    public class Tracer
    {
        private static Tracer instance;
        private readonly TracerConfig _config;
        private readonly TraceEventSession session;
        private readonly ETWTraceEventSource source;
        private RegisteredTraceEventParser _registeredTraceEventParser;

        private Tracer()
        {
            _config = new TracerConfig(
                ConfigurationManager.AppSettings["ProviderName"].ToString(),
                ConfigurationManager.AppSettings["SessionName"].ToString(),
                ConfigurationManager.AppSettings["Storage"].ToString(),
                ConfigurationManager.AppSettings["ClientVersion"].ToString()
                );
            session = new TraceEventSession(_config.SessionName);
            source = new ETWTraceEventSource(_config.SessionName, TraceEventSourceType.Session);
            Filter._version = _config.ClientVersion;
            _registeredTraceEventParser = new RegisteredTraceEventParser(source);
        }

        public static Tracer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Tracer();
                }

                return instance;
            }
        }

        public static string _companyId { get; set; }
        public static string _userName { get; set; }
        public static string _NST { get; set; }


        private void TraceDelegate(TraceEvent data)
        {
            string string1 = "";
            string string2 = "form";

            bool testCheck = data.FormattedMessage.ToLower().Contains(string1.ToLower()) && data.FormattedMessage.ToLower().Contains(string2.ToLower());
            // testCheck = true;
            if (testCheck)
            {
                Console.WriteLine();
                Console.WriteLine(data.FormattedMessage);
                Console.WriteLine();
                Console.WriteLine("------------------------------------------");
            }

            
            //if (Filter.CompanyFilter(data: data))
            //{
            //    if (Filter.GetCompanyName(data: data) != _companyId)
            //    {
            //        _companyId = Filter.GetCompanyName(data: data);
            //    }
            //    else
            //    {
            //        _companyId = _companyId;
            //    }
            //}

            //if (Filter.UserFilter(data: data))
            //{
            //    if (Filter.GetUserName(data: data) != _userName && Filter.GetNSTName(data: data) != _NST)
            //    {
            //        _userName = Filter.GetUserName(data: data);
            //        _NST = Filter.GetNSTName(data: data);
            //    }
            //    else
            //    {
            //        _userName = _userName;
            //        _NST = _NST;
            //    }
            //}

            //if (Filter.PageFilter(data: data) || Filter.ReportFilter(data: data))
            //{
            //    ClientEventBase clientResponse = ClientEventFactory.Create(data: data);

            //    if (clientResponse != null)
            //    {
            //        var result = Storage.PostAsync(clientResponse, _config.Storage);
            //    }
            //}
        }

        public ClientEventBase TraceTest(string data)
        {
            if (Filter.CompanyFilter(testData: data))
            {
                if (Filter.GetCompanyName(testData: data) != _companyId)
                {
                    _companyId = Filter.GetCompanyName(testData: data);
                }
                else
                {
                    _companyId = _companyId;
                }
            }

            if (Filter.UserFilter(testData: data))
            {
                if (Filter.GetUserName(testData: data) != _userName && Filter.GetNSTName(testData: data) != _NST)
                {
                    _userName = Filter.GetUserName(testData: data);
                    _NST = Filter.GetNSTName(testData: data);
                }
                else
                {
                    _userName = _userName;
                    _NST = _NST;
                }
            }

            if (Filter.PageFilter(testData: data) || Filter.ReportFilter(testData: data))
            {
                ClientEventBase clientResponse = ClientEventFactory.Create(testData: data);

                if (clientResponse != null)
                {
                    return clientResponse;                               
                }

                return null;
            }

            return null;
        }

        public void Trace()
        {
            _registeredTraceEventParser.All += TraceDelegate;
            session.EnableProvider(_config.ProviderName);
            source.Process();
        }

        public bool TestConnection()
        {
            if (Storage.TryConnection(_config.Storage) != false)
            {
                return true;
            }
            return false;
        }

        public void Stop()
        {
            _registeredTraceEventParser.All -= TraceDelegate;
        }

        public void DoLogging(bool active, TraceEvent data)
        {
            if (active)
            {
                Console.WriteLine("..................................");
                Console.WriteLine(data.FormattedMessage);
                Console.WriteLine("..................................");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Model
{
    public struct StorageTypes
    {
        public const string Firebase = "Firebase";
        public const string Sql = "Sql";
    }
}

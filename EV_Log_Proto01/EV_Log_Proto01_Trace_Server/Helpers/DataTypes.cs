﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public struct DataTypes
    {
        public const string Page = "Page";
        public const string Report = "Report";
    }
}

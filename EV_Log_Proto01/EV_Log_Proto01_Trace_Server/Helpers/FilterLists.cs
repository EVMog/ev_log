﻿using EV_Log_Proto01_Trace.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public static class FilterLists
    {
        public static readonly List<string> pageFilter01 = new List<string> { "Starting scope", "GetPage" };
        public static readonly List<string> pageFilter02 = new List<string> { "Page" };
        public static readonly List<string> reportOpen = new List<string> { "Starting scope \"Running report processing for report ID" };
        public static readonly List<string> reportClose = new List<string> { "Closing scope \"Running report processing for report ID" };
        public static readonly List<string> companyOpen = new List<string> { "Opening company -- CompanyName:" };

        public static readonly List<string> user = new List<string> { EventTags.UserAuthenticated };
    }
}

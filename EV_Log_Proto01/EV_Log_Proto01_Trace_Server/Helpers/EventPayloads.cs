﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public struct EventPayloads
    {
        public const string CompanyStart = "Opening company -- CompanyName:";
        public const string CompanyEnd = "ProcessId";

        public const string PageStart = "FormId:";
        public const string PageEnd = "ParentFormId";

        public const string ReportStart = "Closing scope \"Running report processing for report ID";
        public const string ReportEnd = "ProcessId";

        public const string ExecutionTimeStart = "Execution time = ";
        public const string ExecutionTimeEnd = ", Exclusive time";

        public const string UserInfoStart = "Message User authenticated. Details - User name: ";
        public const string UserInfoEnd = "; User ID";
    }
}

﻿
namespace EV_Log_Proto01_Trace.Model
{
    public struct EventTags
    {
        public const string Page = "000003Z";
        public const string Report = "0000051";
        public const string Company = "00000KV";
        public const string UserAuthenticated = "0000020";
    }
}

﻿using EV_Log_Proto01_Trace.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace
{
    public class TracerService
    {
        private Tracer _tracer = Tracer.Instance;
        private TracerConfig _config = new TracerConfig();
        private Task _task;
        public bool Start()
        {
            if (_tracer.TestConnection())
            {
                _task = Task.Factory.StartNew(() =>
                {
                    Tracer.Instance.Trace();
                });
                _task.Wait(1);
                return true;
            }
            return false;
        }
        public bool Stop()
        {
            _tracer.Stop();
            return true;
        }
    }
}

﻿using EV_Log_Proto01_Trace.Model;
using Microsoft.Diagnostics.Tracing;
using Microsoft.Diagnostics.Tracing.Parsers;
using Microsoft.Diagnostics.Tracing.Session;
using System;
using Topshelf;

namespace EV_Log_Proto01_Trace
{
    public class Program
    {
        public static void Main(string[] args)
        {
            HostFactory.Run(serviceConfig => {
                serviceConfig.Service<TracerService>(serviceInstance =>
                {
                    serviceInstance.ConstructUsing(
                        () => new TracerService());
                    serviceInstance.WhenStarted(execute => execute.Start());
                    serviceInstance.WhenStopped(execute => execute.Stop());
                });

                serviceConfig.SetServiceName("EVLogServer");
                serviceConfig.SetDisplayName("EV Log Server");
                serviceConfig.SetDescription("Logging of Navision behavior - serverside");
                serviceConfig.StartAutomaticallyDelayed();
            });
        }
    }
}

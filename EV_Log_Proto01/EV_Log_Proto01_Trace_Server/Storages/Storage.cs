﻿using EV_Log_Proto01_Trace.Model;
using EV_Log_Proto01_Trace.Model.Client;
using EV_Log_Proto01_Trace.Storages.Firebase;
using EV_Log_Proto01_Trace.Storages.SQL;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Storages
{
    public static class Storage
    {
        public static FirebaseDB Firebase
        {
            get { return new FirebaseDB(); }
        }

        public static SqlDB Sql
        {
            get { return new SqlDB(); }
        }

        public static async Task<bool> PostAsync(ClientEventBase data, string type)
        {
            switch(type)
            {
                case StorageTypes.Firebase:
                    return await Firebase.PostAsync(data);
                case StorageTypes.Sql:
                    return await Sql.PostAsync(data);
                default:
                    return false;
            }
        }

        public static bool TryConnection(string type)
        {
            switch (type)
            {
                case StorageTypes.Firebase:
                    return Firebase.TryConnection();
                case StorageTypes.Sql:
                    return Sql.TryConnection();
                default:
                    return false;
            }
        }
    }
}

﻿using EV_Log_Proto01_Trace.Model.Client;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Storages.SQL
{
    public class SqlDB
    {
        private readonly string connectionString =
            "Data Source=NSTEventListene\\BCDEMO;" +
            "Initial Catalog=EV_Log_TestDB;" +
            "Integrated Security=True;" +
            "Connect Timeout=30;" +
            "Encrypt=False;" +
            "TrustServerCertificate=False;" +
            "ApplicationIntent=ReadWrite;" +
            "MultiSubnetFailover=False";

        public SqlDB() { }

        public async Task<bool> PostAsync(ClientEventBase data)
        {
            if (data.Type == "Page")
            {
                var castData = (ClientPageEvent)data;

                using (SqlConnection c = new SqlConnection(connectionString))
                {
                    using (SqlCommand query = new SqlCommand("INSERT INTO TracePages (CompanyID, NST, PageID, Timestamp, Type, UserID) VALUES" +
                                                             "(@CompanyID, @NST, @PageID, @Timestamp, @Type, @UserID)", c))
                    {
                        query.Parameters.AddWithValue("@CompanyID", castData.CompanyID);
                        query.Parameters.AddWithValue("@NST", castData.NST);
                        query.Parameters.AddWithValue("@PageID", castData.PageID);
                        query.Parameters.AddWithValue("@Timestamp", castData.Timestamp);
                        query.Parameters.AddWithValue("@Type", castData.Type);
                        query.Parameters.AddWithValue("@UserID", castData.UserID);

                        await c.OpenAsync();
                        var result = await query.ExecuteNonQueryAsync();
                        c.Close();

                        if (result == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            else if (data.Type == "Report")
            {
                var castData = (ClientReportEvent)data;

                using (SqlConnection c = new SqlConnection(connectionString))
                {
                    using (SqlCommand query = new SqlCommand("INSERT INTO TraceReports (CompanyID, NST, ReportID, Timestamp, Type, UserID, EventStart, ExecutionTime) VALUES" +
                                                             "(@CompanyID, @NST, @ReportID, @Timestamp, @Type, @UserID, @EventStart, @ExecutionTime)", c))
                    {
                        query.Parameters.AddWithValue("@CompanyID", castData.CompanyID);
                        query.Parameters.AddWithValue("@NST", castData.NST);
                        query.Parameters.AddWithValue("@ReportID", castData.ReportID);
                        query.Parameters.AddWithValue("@Timestamp", castData.Timestamp);
                        query.Parameters.AddWithValue("@Type", castData.Type);
                        query.Parameters.AddWithValue("@UserID", castData.UserID);
                        query.Parameters.AddWithValue("@EventStart", castData.EventStart);
                        query.Parameters.AddWithValue("@ExecutionTime", castData.ExecutionTime);

                        await c.OpenAsync();
                        var result = await query.ExecuteNonQueryAsync();
                        c.Close();

                        if (result == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            else
            {
                return false;
            }
        }
        
        public bool TryConnection()
        {
            try
            {
                using (SqlConnection c = new SqlConnection(connectionString))
                {
                    using (SqlCommand query = new SqlCommand("INSERT INTO TracePages (CompanyID, NST, PageID, Timestamp, Type, UserID) VALUES" +
                                                             "(@CompanyID, @NST, @PageID, @Timestamp, @Type, @UserID)", c))
                    {
                        query.Parameters.AddWithValue("@CompanyID", "TestCompany");
                        query.Parameters.AddWithValue("@NST", "TestNST");
                        query.Parameters.AddWithValue("@PageID", "TestPage");
                        query.Parameters.AddWithValue("@Timestamp", DateTime.Now);
                        query.Parameters.AddWithValue("@Type", "TestType");
                        query.Parameters.AddWithValue("@UserID", "TestUser");

                        c.Open();
                        var result = query.ExecuteNonQuery();

                        if (result == 1)
                        {
                            SqlCommand command = new SqlCommand("DELETE FROM TracePages WHERE CompanyID = @CompanyID", c);
                            command.Parameters.AddWithValue("@CompanyID", "TestCompany");

                            command.ExecuteNonQuery();
                            c.Close();
                            return true;
                        }
                        else
                        {
                            c.Close();
                            return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}

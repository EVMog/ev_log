﻿using EV_Log_Proto01_Trace.Helpers;
using EV_Log_Proto01_Trace.Model.Client;
using EV_Log_Proto01_Trace.Model.Server;
using FireSharp.Config;
using FireSharp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Storages.Firebase
{
    public class FirebaseDB
    {
        private readonly IFirebaseConfig config = new FirebaseConfig { AuthSecret = "YSPQWy6cDZIvar20pees3hBXFrXllyyZ4ekxOsNh", BasePath = "https://ev-log.firebaseio.com/" };
        private readonly IFirebaseClient client;

        public FirebaseDB()
        {
            client = new FireSharp.FirebaseClient(config);
        }

        public async Task<bool> PostAsync(ClientEventBase data)
        {
            FireSharp.Response.PushResponse response;

            switch (data.Type)
            {
                case DataTypes.Page:
                    response = await client.PushTaskAsync(data.NST + "/pages/" + data.UserID, data);
                    break;
                case DataTypes.Report:
                    response = await client.PushTaskAsync(data.NST + "/reports/" + data.UserID, data);
                    break;
                default:
                    response = null;
                    break;
            }

            if (response != null)
            {
                return response.Result.Name != null ? true : false;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> PostServerAsync(ServerEventBase data)
        {
            var response = await client.PushTaskAsync("users/" + data.Key, data);
            return response.Result.Name != null ? true : false;
        }

        public async Task<bool> PostEverythingServerAsync(ServerEventBase data)
        {
            var response = await client.PushTaskAsync("serverEvents", data);
            return response.Result.Name != null ? true : false;
        }

        public bool ClearCollection()
        {
            var response = client.Delete("events");
            return response.Success;
        }

        public bool TryConnection()
        {
            try
            {
                var tryPush = client.Push("", new { connectionStatus = "Testing" });
                var key = tryPush.Result.Name;
                var tryDelete = client.Delete("" + key);
                return tryDelete.Success;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}

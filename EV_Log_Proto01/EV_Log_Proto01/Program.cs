﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using System.Threading;

namespace EV_Log_Proto01
{
    public class Program
    {
        public static readonly EventSource MyLogger = new EventSource("Microsoft-EtwDemo");

        public static void Main(string[] args)
        {
            Console.WriteLine("Creating custom events...");
            var random = new Random();

            while (true)
            {
                MyLogger.Write(
                    "EventName",
                    new EventSourceOptions { Level = EventLevel.Informational },
                    new
                    {
                        PageID = random.Next(1,100),
                        CompanyID = random.Next(1, 20),
                        UserID = random.Next(1, 5)
                    });
                Thread.Sleep(2000);
            }
        }
    }
}
﻿using EV_Log_Proto01_Trace.Helpers;

namespace EV_Log_Proto01_Trace.Model
{
    public class TracerConfig
    {
        public TracerConfig()
        {
            ProviderName = EventProviders.Client;
            SessionName = "Realtime Tracing Session";
            Storage = StorageTypes.Firebase;
            ClientVersion = ClientVersions.v14_037587_0;
        }

        public TracerConfig(string providerName, string sessionName, string storage, string version)
        {
            ProviderName = providerName;
            SessionName = sessionName;
            Storage = storage;
            ClientVersion = version;
        }

        public string ProviderName { get; set; }
        public string SessionName { get; set; }
        public string Storage { get; set; }
        public string ClientVersion { get; set; }
    }
}

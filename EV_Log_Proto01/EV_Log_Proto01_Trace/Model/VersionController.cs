﻿using EV_Log_Proto01_Trace.Helpers;
using EV_Log_Proto01_Trace.Versioning;
using System;

namespace EV_Log_Proto01_Trace.Model
{
    public static class VersionController
    {
        private static string version = Tracer.TracerConfig.ClientVersion;
        public static void Initializer()
        {
            if (version.IsBiggerThanOrEqual(ClientVersions.v11_0_31747_0) && !version.IsBiggerThanOrEqual(ClientVersions.v14_037587_0))
            {
                // TODO FIX this for version 11
                //List<string> NstUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('/').ToList();

                //Tracer.NST = NstUser[0];
                //Tracer.UserName = NstUser[1];
            }
        }

        public static IVersion Version()
        {
            if (version.IsBiggerThanOrEqual(ClientVersions.v14_037587_0))
            {
                return new Version14_037587_0();
            }
            if (version.IsBiggerThanOrEqual(ClientVersions.v11_0_31747_0))
            {
                return new Version11_0_31747_0();
            }
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EV_Log_Proto01_Trace.Model
{
    public static class VersionExtension
    {
        public static bool IsBiggerThanOrEqual(this string thisString, string otherString)
        {
            bool resultFound = false;
            bool result = false;

            List<string> thisStringList = thisString.Split('.').ToList();
            List<string> otherStringList = otherString.Split('.').ToList();
            List<int> thisIntList = new List<int>();
            List<int> otherIntList = new List<int>();

            try
            {
                thisStringList.ForEach(str => thisIntList.Add(Int32.Parse(str)));
                otherStringList.ForEach(str => otherIntList.Add(Int32.Parse(str)));
            }
            catch (FormatException)
            {
                return false;               
            }

            while (resultFound == false)
            {
                Compare();
            }

            void Compare()
            {
                if (thisIntList.Count() == 0 && otherIntList.Count() > 0)
                {
                    result = false;
                    resultFound = true;
                }
                if (otherIntList.Count() == 0 && thisIntList.Count() > 0)
                {
                    result = true;
                    resultFound = true;
                }
                if (otherIntList.Count() == 0 && thisIntList.Count() == 0 )
                {
                    result = true;
                    resultFound = true;
                }

                if (thisIntList.Count >0 && otherIntList.Count > 0)
                {
                    if (thisIntList.First() > otherIntList.First())
                    {
                        result = true;
                        resultFound = true;
                    }
                    if (thisIntList.First() < otherIntList.First())
                    {
                        result = false;
                        resultFound = true;
                    }
                }

                if (thisIntList.Count >0)
                {
                    thisIntList.RemoveAt(0);
                }
                if (otherIntList.Count >0)
                {
                    otherIntList.RemoveAt(0);
                }
                
            }
            return result;
        }
    }
}

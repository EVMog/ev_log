﻿using EV_Log_Proto01_Trace.Helpers;
using Microsoft.Diagnostics.Tracing;
using System.Collections.Generic;

namespace EV_Log_Proto01_Trace.Model
{
    public static class Filter
    {

        public static bool StandardFilter(List<string> filterValues, TraceEvent data)
        {
            for (var i = 0; i < filterValues.Count; i++)
            {
                if (!data.FormattedMessage.ToUpper().Contains(filterValues[i].ToUpper()))
                {
                    return false;
                }
            }
            return true;
        }

        public static string GetExecutionTime(TraceEvent data)
        {   string result = GetPayload(data.FormattedMessage, EventPayloads.ExecutionTimeStart, EventPayloads.ExecutionTimeEnd);
            try
            {
                long.Parse(result);
            }
            catch (System.Exception)
            {
                result = "-1";
            }
            return result;
        }
       

        public static string GetPayload(string data, string start, string end)
        {
            try
            {
                string dataUpper = data.ToUpper();
                start = start.ToUpper();
                end = end.ToUpper();

                char[] trims = { '\n', '\r', ' ' };
                int Start = dataUpper.IndexOf(start);
                Start = Start + start.Length;
                int Length;
                if (end == "END")
                {
                    Length = dataUpper.Length -1 - Start;
                }
                else
                {
                    int endindex = dataUpper.IndexOf(end);
                    Length = endindex - Start;
                }

                return data.Substring(Start, Length).Trim(trims);
            }
            catch (System.Exception)
            {

                return "could not get Payload from TraceEvent";
            }
            
        }
    }
}

﻿using EV_Log_Proto01_Trace.Model.Client;
using EV_Log_Proto01_Trace.Model.Events;
using EV_Log_Proto01_Trace.Storages;
using EV_Log_Proto01_Trace.Storages.AppInsights;
using EV_Log_Proto01_Trace.Versioning;
using Microsoft.Diagnostics.Tracing;
using Microsoft.Diagnostics.Tracing.Parsers;
using Microsoft.Diagnostics.Tracing.Session;
using System;
using System.Configuration;
using System.Net;

namespace EV_Log_Proto01_Trace.Model
{
    public class Tracer
    {
        private static Tracer instance;

        private readonly TraceEventSession _session;
        private readonly ETWTraceEventSource _source;
        private RegisteredTraceEventParser _registeredTraceEventParser;
        private ApplicationInsights _applicationInsights;


        private Tracer()
        {
            Version = VersionController.Version();
            _session = new TraceEventSession(TracerConfig.SessionName);
            _source = new ETWTraceEventSource(TracerConfig.SessionName, TraceEventSourceType.Session);
            _registeredTraceEventParser = new RegisteredTraceEventParser(_source);
            VersionController.Initializer();
            IpAddress = GetIPAddress();
            _applicationInsights = new ApplicationInsights(false, true, true); //TODO: Fix parameters before launch
        }

        public static Tracer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Tracer();
                }
                return instance;
            }
        }
        public static string IpAddress { get; set; }
        public static string NST { get; set; }
        public static string CompanyId { get; set; }
        public static string UserName { get; set; }
        public static TracerConfig TracerConfig { get; } = new TracerConfig(
                ConfigurationManager.AppSettings["ProviderName"].ToString(),
                ConfigurationManager.AppSettings["SessionName"].ToString(),
                ConfigurationManager.AppSettings["Storage"].ToString(),
                ConfigurationManager.AppSettings["ClientVersion"].ToString()
                );
        public static TraceEvent Data { get; set; }

        public IVersion Version { get; }


        private void TraceDelegate(TraceEvent data)
        {

            Data = data;
            if (NST == null)
            {
                NST = Version.GetNST;
            }
           
            bool userFilter = data.FormattedMessage.Contains("0000020") && !data.FormattedMessage.Contains("<redacted>");
            bool companyFilter = data.FormattedMessage.Contains("Opening company -- CompanyName:") && !data.FormattedMessage.Contains("ClientSessionId: 00000000-0000-0000-0000-000000000000");
            bool validateFieldFilter = data.FormattedMessage.Contains("Closing scope") && data.FormattedMessage.Contains("ValidateField");
            bool pageFilter = data.FormattedMessage.Contains("Closing scope \"GetPage");
            bool currentRecordFilter = data.FormattedMessage.Contains("InvokeApplicationMethod") && data.FormattedMessage.Contains("AfterGetCurrRecord") && data.FormattedMessage.Contains("Result = Success");
            bool reportFilter = data.FormattedMessage.Contains("0000051") && data.FormattedMessage.Contains("Closing scope");
            bool lockedByUserFilter = data.FormattedMessage.Contains("Long running SQL statement") && (data.FormattedMessage.Contains("locked by another user") || data.FormattedMessage.Contains("låst af en anden bruger"));
            bool longRunningFilter = data.FormattedMessage.Contains("Long running SQL statement") && !data.FormattedMessage.Contains("locked by another user") && !data.FormattedMessage.Contains("låst af en anden bruger");
            bool formFilter = data.FormattedMessage.Contains("Closing scope \"CloseForm");
            bool actionFilter = data.FormattedMessage.Contains("Closing scope \"ActionField");
           
            ConditionalPost(userFilter, EventFactory.CreateUserEvent);
            ConditionalPost(companyFilter, EventFactory.CreateCompanyEvent);
            ConditionalPost(validateFieldFilter, EventFactory.CreateValidateFieldEvent);
            ConditionalPost(pageFilter, EventFactory.CreatePageEvent);
            ConditionalPost(currentRecordFilter, EventFactory.CreateCurrentRecordEvent);
            ConditionalPost(reportFilter, EventFactory.CreateReportEvent);
            ConditionalPost(formFilter, EventFactory.CreateFormEvent);
            ConditionalPost(lockedByUserFilter, EventFactory.CreateSQLLockedByUserEvent);
            ConditionalPost(longRunningFilter, EventFactory.CreateSQLLongRunningEvent);
            ConditionalPost(actionFilter, EventFactory.CreateActionEvent);

        }


        public void Trace()
        {
            _registeredTraceEventParser.All += TraceDelegate;
            _session.EnableProvider(TracerConfig.ProviderName);
            _source.Process();
        }

        public bool TestConnection()
        {
            if (Storage.TryConnection(TracerConfig.Storage) != false)
            {
                return true;
            }
            return false;
        }

        public void Stop()
        {
            _registeredTraceEventParser.All -= TraceDelegate;
        }

        public void DoLogging(bool active, TraceEvent data)
        {
            if (active)
            {
                Console.WriteLine("..................................");
                Console.WriteLine(data.FormattedMessage);
                Console.WriteLine("..................................");
            }
        }
        public void ConditionalPost(bool filter, Func<TraceEvent, EventBase> factoryMethod)
        {
            if (filter == true)
            {
                EventBase response = factoryMethod(Data);
                if (EventValidater.ValidateTraceEvent(Data))
                {
                    _applicationInsights.Post(response);
                    return;
                }
            }
        }
        private static string GetIPAddress()
        {
            string result = "";
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    result = Convert.ToString(IP);
                }
            }
            return result;
        }
    }
}

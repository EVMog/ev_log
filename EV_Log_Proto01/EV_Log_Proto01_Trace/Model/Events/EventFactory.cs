﻿using EV_Log_Proto01_Trace.Helpers;
using EV_Log_Proto01_Trace.Model.Events;
using Microsoft.Diagnostics.Tracing;
using System;

namespace EV_Log_Proto01_Trace.Model.Client
{
    public static class EventFactory
    {

        public static CompanyEvent CreateCompanyEvent(TraceEvent data)
        {
            return new CompanyEvent(
                data.TimeStamp,
                DataCategories.Runtime,
                EventLabels.OpeningCompany,
                Tracer.IpAddress,
                Tracer.NST,
                Tracer.Instance.Version.GetClientSessionId,
                EventTags.T00000SL,
                EventObjectTypes.Company,
                Tracer.Instance.Version.GetCompany
                );
        }
        public static CurrentRecordEvent CreateCurrentRecordEvent(TraceEvent data)
        {
            return new CurrentRecordEvent(
                data.TimeStamp,
                DataCategories.Management,
                EventLabels.AfterGetCurrRecord,
                Tracer.IpAddress,
                Tracer.NST,
                Tracer.Instance.Version.GetClientSessionId,
                EventTags.T000003Z,
                EventObjectTypes.CurrentRecord,
                Tracer.Instance.Version.GetCurrentRecord,
                Filter.GetExecutionTime(data),
                CalcStartTime(data.TimeStamp, Filter.GetExecutionTime(data))
                );
        }
        public static FormEvent CreateFormEvent(TraceEvent data)
        {
            string allFormIds = Tracer.Instance.Version.GetFormId;
            FormEvent resultForm = new FormEvent(
            data.TimeStamp,
            DataCategories.Management,
            EventLabels.ClosingScopeOpenForm,
            Tracer.IpAddress,
            Tracer.NST,
            Tracer.Instance.Version.GetClientSessionId,
            EventTags.T000003Z,
            EventObjectTypes.Form,
            GetObjectId(),
            Filter.GetExecutionTime(data),
            CalcStartTime(data.TimeStamp, Filter.GetExecutionTime(data)),
            GetSubObjectIds()

            );
            string GetObjectId()
            {
                if (allFormIds.Contains(","))
                {
                    string result = allFormIds.Substring(0, allFormIds.IndexOf(','));
                    return result;
                }
                allFormIds = allFormIds.Replace("\r\n\".", "");
                allFormIds = allFormIds.Replace(" ", "");
                return allFormIds;
            }

            string[] GetSubObjectIds()
            {
                if (allFormIds.Contains(","))
                {
                    allFormIds = allFormIds.Replace("\r\n\".", "");
                    allFormIds = allFormIds.Replace(" ", "");
                    int posOfFirstComma = allFormIds.IndexOf(',');
                    char[] splitSeperator = { ',' };
                    allFormIds = allFormIds.Substring(posOfFirstComma + 1, allFormIds.Length - posOfFirstComma - 1);
                    string[] result = allFormIds.Split(splitSeperator);
                    return result;
                }
                else return null;
                
            }

            return resultForm;
        }

        public static PageEvent CreatePageEvent(TraceEvent data)
        {
            return new PageEvent(
                data.TimeStamp,
                DataCategories.Management,
                EventLabels.ClosingScopeOpenForm,
                Tracer.IpAddress,
                Tracer.NST,
                Tracer.Instance.Version.GetClientSessionId,
                EventTags.T000003Z,
                EventObjectTypes.Page,
                Tracer.Instance.Version.GetPage,
                Filter.GetExecutionTime(data),
                CalcStartTime(data.TimeStamp, Filter.GetExecutionTime(data))

                );
        }
        public static ReportEvent CreateReportEvent(TraceEvent data)
        {
            return new ReportEvent(
                 data.TimeStamp,
                 DataCategories.Management,
                 EventLabels.ClosingScopeRunReport,
                 Tracer.IpAddress,
                 Tracer.NST,
                 Tracer.Instance.Version.GetClientSessionId,
                 EventTags.T0000051,
                 EventObjectTypes.Report,
                 Tracer.Instance.Version.GetReport,
                 Filter.GetExecutionTime(data),
                 CalcStartTime(data.TimeStamp, Filter.GetExecutionTime(data))

                 );
        }
        public static SQLLockedByUserEvent CreateSQLLockedByUserEvent(TraceEvent data)
        {
            string FormatedMessage = data.FormattedMessage.Replace("\" SET", "$SET");
            string executionTime = Filter.GetPayload(data.FormattedMessage, "Execution time: ", " ms,");
            string tableName = Filter.GetPayload(FormatedMessage, "CURRENTCOMPANY$", "$SET");
            string message = Filter.GetPayload(data.FormattedMessage, "AL CallStack:", "end");
            return new SQLLockedByUserEvent(
                 data.TimeStamp,
                 DataCategories.Sql,
                 EventLabels.NavCSideException,
                 Tracer.IpAddress,
                 Tracer.NST,
                 Tracer.Instance.Version.GetClientSessionId,
                 EventTags.T000007L,
                 EventObjectTypes.SQL_LockedByUser,
                 Tracer.Instance.Version.GetSQL_LockedByUserAppObjectId,
                 executionTime,
                 CalcStartTime(data.TimeStamp, executionTime),
                 message,
                 tableName,
                 Tracer.Instance.Version.GetSQLObjectType

                 );
        }
        public static SQLLongRunningEvent CreateSQLLongRunningEvent(TraceEvent data)
        {
            string FormatedMessage = data.FormattedMessage.Replace("\" SET", "$SET");
            string executionTime = Filter.GetPayload(data.FormattedMessage, "Execution time: ", " ms,");
            string tableName = Filter.GetPayload(FormatedMessage, "CURRENTCOMPANY$", "$SET");
            string message = Filter.GetPayload(data.FormattedMessage, "AL CallStack:", "end");
            return new SQLLongRunningEvent(
                 data.TimeStamp,
                 DataCategories.Sql,
                 EventLabels.NavCSideException,
                 Tracer.IpAddress,
                 Tracer.NST,
                 Tracer.Instance.Version.GetClientSessionId,
                 EventTags.T000007L,
                 EventObjectTypes.SQL_LongRunning,
                 Tracer.Instance.Version.GetSQL_LongRunningAppObjectId,
                 executionTime,
                 CalcStartTime(data.TimeStamp, executionTime),
                 message,
                 tableName,
                 Tracer.Instance.Version.GetSQLObjectType
                 );
        }
        public static UserEvent CreateUserEvent(TraceEvent data)
        {
            return new UserEvent(
                data.TimeStamp,
                DataCategories.Security,
                EventLabels.UserAuthenticated,
                Tracer.IpAddress,
                Tracer.NST,
                Tracer.Instance.Version.GetClientSessionId,
                EventTags.T0000020,
                EventObjectTypes.User,
                Tracer.Instance.Version.GetUser
                );
        }
        public static ValidateFieldEvent CreateValidateFieldEvent(TraceEvent data)
        {
            return new ValidateFieldEvent(
                 data.TimeStamp,
                 DataCategories.Management,
                 EventLabels.ClosingAcopeValidateField,
                 Tracer.IpAddress,
                 Tracer.NST,
                 Tracer.Instance.Version.GetClientSessionId,
                 EventTags.T000003Z,
                 EventObjectTypes.ValidateField,
                 Tracer.Instance.Version.GetValidateField,
                 Filter.GetExecutionTime(data),
                 CalcStartTime(data.TimeStamp, Filter.GetExecutionTime(data)),
                 Tracer.Instance.Version.GetControlId
                 );
        }
        public static ActionEvent CreateActionEvent(TraceEvent data)
        {
            return new ActionEvent(
                data.TimeStamp,
                DataCategories.Management,
                EventLabels.ClosingScopeActionField,
                Tracer.IpAddress,
                Tracer.NST,
                Tracer.Instance.Version.GetClientSessionId,
                EventTags.T000003Z,
                EventObjectTypes.Action,
                Tracer.Instance.Version.GetAction,
                Filter.GetExecutionTime(data),
                CalcStartTime(data.TimeStamp, Filter.GetExecutionTime(data)),
                Tracer.Instance.Version.GetControlId

                );
        }

        private static DateTime CalcStartTime(DateTime end, string executionTime)
        {
            try
            {
                return end.AddMilliseconds(0 - Convert.ToDouble(executionTime));
            }
            catch (Exception)
            {

                return default;
            }
           
        }
    }
}

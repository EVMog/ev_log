﻿using EV_Log_Proto01_Trace.Model.Events;
using System;

namespace EV_Log_Proto01_Trace.Model.Client
{
    public class PageEvent : EventBasePlus
    {
        public PageEvent(DateTime timestamp, string category, string traceEvent, string ipAddress, string serverInstance, string clientSessionId, string tag, string objectType, string objectId, string executionTime, DateTime eventStart) : 
            base(timestamp, category, traceEvent, ipAddress, serverInstance, clientSessionId, tag, objectType, objectId, executionTime, eventStart)
        {
        }
    }
}

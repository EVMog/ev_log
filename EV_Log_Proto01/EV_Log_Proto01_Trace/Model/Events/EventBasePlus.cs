﻿using EV_Log_Proto01_Trace.Model.Client;
using System;

namespace EV_Log_Proto01_Trace.Model.Events
{
    public class EventBasePlus : EventBase
    {
        public EventBasePlus(DateTime timestamp, string category, string traceEvent, string ipAddress, string serverInstance, string clientSessionId, string tag, string objectType, string objectId, string executionTime, DateTime eventStart) 
            : base(timestamp, category, traceEvent, ipAddress, serverInstance, clientSessionId, tag, objectType)
        {
            
            ObjectId = objectId;
            ExecutionTime = executionTime;
            EventStart = eventStart;
        }

        
        public string ObjectId { get; set; }
        public string ExecutionTime { get; set; }
        public DateTime EventStart { get; set; }

    }
}

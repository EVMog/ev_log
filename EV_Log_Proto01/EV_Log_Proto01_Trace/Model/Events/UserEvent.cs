﻿using EV_Log_Proto01_Trace.Model.Client;
using System;

namespace EV_Log_Proto01_Trace.Model.Events
{
    public class UserEvent : EventBase
    {
        public UserEvent(DateTime timestamp, string category, string traceEvent, string ipAddress, string serverInstance, string clientSessionId, string tag, string objectType, string user) 
            : base(timestamp, category, traceEvent, ipAddress, serverInstance, clientSessionId, tag, objectType)
        {
            User = user;
        }
        public string User { get; set; }
    }
}

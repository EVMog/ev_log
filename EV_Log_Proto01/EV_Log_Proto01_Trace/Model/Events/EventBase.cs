﻿using System;

namespace EV_Log_Proto01_Trace.Model.Client
{
    public abstract class EventBase
    {
        public EventBase(DateTime timestamp, string category, string traceEvent, string ipAddress, string serverInstance, string clientSessionId, string tag, string objectType)
        {
            Timestamp = timestamp;
            Category = category;
            Event = traceEvent;
            IpAddress = ipAddress;
            ServerInstance = serverInstance;
            ClientSessionId = clientSessionId;
            Tag = tag;
            ObjectType = objectType;


        }
        public DateTime Timestamp { get; set; }
        public string Category { get; set; }
        public string Event { get; set; }
        public string IpAddress { get; set; }
        public string ServerInstance { get; set; }
        public string ClientSessionId { get; set; }
        public string Tag { get; set; }
        public string ObjectType { get; set; }

    }
}

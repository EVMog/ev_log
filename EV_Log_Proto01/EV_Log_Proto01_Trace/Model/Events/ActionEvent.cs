﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Model.Events
{
    public class ActionEvent : EventBaseControlId
    {
        public ActionEvent(DateTime timestamp, string category, string traceEvent, string ipAddress, string serverInstance, string clientSessionId, string tag, string objectType, string objectId, string executionTime, DateTime eventStart, string controlId) : 
            base(timestamp, category, traceEvent, ipAddress, serverInstance, clientSessionId, tag, objectType, objectId, executionTime, eventStart, controlId)
        {
        }
    }
}

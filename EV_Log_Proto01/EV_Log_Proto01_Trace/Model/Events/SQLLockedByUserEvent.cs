﻿using System;

namespace EV_Log_Proto01_Trace.Model.Events
{
    public class SQLLockedByUserEvent : EventBasePlus
    {
        public SQLLockedByUserEvent(DateTime timestamp, string category, string traceEvent, string ipAddress, string serverInstance, string clientSessionId, string tag, string objectType, string objectId, string executionTime, DateTime eventStart, string message, string sqlTableName, string sqlObjectType) 
            : base(timestamp, category, traceEvent, ipAddress, serverInstance, clientSessionId, tag, objectType, objectId, executionTime, eventStart)
        {
            Message = message;
            SqlTableName = sqlTableName;
            SqlObjectType = sqlObjectType;
        }
        public string Message { get; set; }
        public string SqlTableName { get; set; }
        public string SqlObjectType { get; set; }
    }
}

﻿using EV_Log_Proto01_Trace.Model.Client;
using System;

namespace EV_Log_Proto01_Trace.Model.Events
{
    public class CompanyEvent : EventBase
    {
        public CompanyEvent(DateTime timestamp, string category, string traceEvent, string ipAddress, string serverInstance, string clientSessionId, string tag, string objectType, string companyId) 
            : base(timestamp, category, traceEvent, ipAddress, serverInstance, clientSessionId, tag, objectType)
        {
            CompanyId = companyId;
        }

        public string CompanyId { get; set; }
    }
}

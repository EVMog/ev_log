﻿using EV_Log_Proto01_Trace.Helpers;
using EV_Log_Proto01_Trace.Model;
using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Versioning
{
    public class Version14_037587_0 : IVersion
    {
        public string GetCompany => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.CompanyStart, EventPayloads.CompanyEnd);
        public string GetPage => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.PageStart, EventPayloads.PageEnd);
        public string GetReport => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.ReportStart, EventPayloads.ReportEnd).Trim('"', '.');
        public string GetUser => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.UserInfoStart, EventPayloads.UserInfoEnd);
        public string GetNST => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.NstStart, EventPayloads.NstEnd);

        public string GetClientSessionId => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.ClientSessionIdStart, EventPayloads.ClientSessionIdEnd);

        public string GetCurrentRecord => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.AfterGetCurrentRecordIdStart, EventPayloads.AfterGetCurrentRecordIdEnd);

        public string GetSQL_LockedByUserMessage => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.LockedByUserMessageStart, EventPayloads.LockedByUserMessageEnd);

        public string GetSQL_LongRunningMessage => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.LongRunningMessageStart, EventPayloads.LongRunningMessageEnd);

        public string GetSQL_LockedByUserAppObjectId => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.SqlLongRunningAppObjectIdStart, EventPayloads.SqlLongRunningAppObjectIdEnd);

        public string GetSQL_LongRunningAppObjectId => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.SqlLongRunningAppObjectIdStart, EventPayloads.SqlLongRunningAppObjectIdEnd);

        public string GetControlId => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.ControlIdStart, EventPayloads.ControlIdEnd);

        public string GetValidateField => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.ValidateFieldIdStart, EventPayloads.ValidateFieldIdEnd);

        public string GetSQLTableName => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.SqlTableNameStart, EventPayloads.SqlTableNameEnd);

        public string GetFormId => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.FormIdStart, EventPayloads.FormIdEnd);

        public string GetAction => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.ActionIdStart, EventPayloads.ActionIdEnd);

        public string GetSQLObjectType => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.SQLGetObjectTypeStart, EventPayloads.SQLGetObjectTypeEnd);

        public bool ActionFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool CompanyFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.companyOpen, data);
        }

        public bool CurrentRecordFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.CurrentRecordFilter, data);
        }

        public bool FormFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool NstFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.NstFilter, data);
        }

        public bool PageFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.pageClose, data);
        }

        public bool ReportFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.reportClose, data);
        }

        public bool SQLLockedByUserFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.SQLLockedByUserFilter, data);
        }

        public bool SQLLongRunningFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.SQLLongRunningFilter, data);
        }

        public bool UserFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.user, data);
        }

        public bool ValidateFieldFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.ValidateFieldFilter, data);
        }
    }
}

﻿using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Versioning
{
    public interface IVersion
    {
        bool CompanyFilter(TraceEvent data);
        bool UserFilter(TraceEvent data);
        bool ReportFilter(TraceEvent data);
        bool PageFilter(TraceEvent data);
        bool NstFilter(TraceEvent data);
        bool ValidateFieldFilter(TraceEvent data);
        bool CurrentRecordFilter(TraceEvent data);
        bool FormFilter(TraceEvent data);
        bool SQLLockedByUserFilter(TraceEvent data);
        bool SQLLongRunningFilter(TraceEvent data);
        bool ActionFilter(TraceEvent data);


        string GetCompany { get; }
        string GetPage { get; }
        string GetReport { get;}
        string GetUser { get; }
        string GetNST { get; }
        string GetClientSessionId { get; }
        string GetCurrentRecord { get; }
        string GetSQL_LockedByUserMessage { get; }
        string GetSQL_LongRunningMessage { get; }
        string GetSQL_LockedByUserAppObjectId { get; }
        string GetSQL_LongRunningAppObjectId { get; }
        string GetControlId { get; }
        string GetValidateField { get; }
        string GetSQLTableName { get; }
        string GetFormId { get; }
        string GetAction { get; }
        string GetSQLObjectType { get; }
      



    }
}

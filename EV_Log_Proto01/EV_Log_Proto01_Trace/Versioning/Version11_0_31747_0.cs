﻿using EV_Log_Proto01_Trace.Helpers;
using EV_Log_Proto01_Trace.Model;
using Microsoft.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Versioning
{
    public class Version11_0_31747_0 : IVersion

    {
        public string GetCompany => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.CompanyStart, EventPayloads.CompanyEnd).Substring(4, Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.CompanyStart, EventPayloads.CompanyEnd).Length - 5);
        public string GetPage => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.PageStart2, EventPayloads.PageEnd2).Trim('"', '.');
        public string GetReport => Filter.GetPayload(Tracer.Data.FormattedMessage, EventPayloads.ReportStart2, EventPayloads.ReportEnd).Trim('"', '.');
        public string GetUser => throw new NotImplementedException(); // TODO FIX OLD VERSION: Tracer.NST + '/' + Tracer.UserName;
        public string GetNST => Tracer.NST;

        public string GetClientSessionId => throw new NotImplementedException();

        public string GetCurrentRecord => throw new NotImplementedException();

        public string GetSQL_LockedByUserMessage => throw new NotImplementedException();

        public string GetSQL_LongRunningMessage => throw new NotImplementedException();

        public string GetSQL_LockedByUserAppObjectId => throw new NotImplementedException();

        public string GetSQL_LongRunningAppObjectId => throw new NotImplementedException();

        public string GetControlId => throw new NotImplementedException();

        public string GetValidateField => throw new NotImplementedException();

        public string GetSQLTableName => throw new NotImplementedException();

        public string GetFormId => throw new NotImplementedException();

        public string GetAction => throw new NotImplementedException();

        string IVersion.GetSQLObjectType => throw new NotImplementedException();

        public bool ActionFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool CompanyFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.companyOpen2, data);
        }

        public bool CurrentRecordFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool FormFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool GetSQLObjectType(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool NstFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool PageFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.pageFilter02, data);
        }

        public bool ReportFilter(TraceEvent data)
        {
            return Filter.StandardFilter(FilterLists.reportClose2, data);
        }

        public bool SQLLockedByUserFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool SQLLongRunningFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }

        public bool UserFilter(TraceEvent data)
        {
            return false;
        }

        public bool ValidateFieldFilter(TraceEvent data)
        {
            throw new NotImplementedException();
        }
    }
}

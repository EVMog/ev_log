﻿using EV_Log_Proto01_Trace.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace EV_Log_Proto01_Trace
{
    public class TracerService
    {
        private Tracer _tracer = Tracer.Instance;
        // private TracerConfig _config = new TracerConfig();
        private Task _traceTask;
        private Task _dataMergeTask;
        public bool Start()
        {
            if (_tracer.TestConnection())
            {
                _traceTask = Task.Factory.StartNew(() =>
                {
                    Tracer.Instance.Trace();
                });

                // Remove datamergetask when presentation is done.
                _dataMergeTask = Task.Factory.StartNew(() =>
                {
                    System.Timers.Timer updateTimer = new System.Timers.Timer(60 * 60 * 1000); //one hour in milliseconds - TODO: add configurable multiplier
                    updateTimer.Elapsed += new ElapsedEventHandler(OnUpdateTimerEvent);
                    updateTimer.Start();
                });
                _traceTask.Wait(1);
                _dataMergeTask.Wait(1);
                return true;
            }
            return false;
        }

        private static void OnUpdateTimerEvent(object source, ElapsedEventArgs e)
        {
            // run api merge method here ( will run every interval of updateTimer)
        }
        public bool Stop()
        {
            _tracer.Stop();
            return true;
        }
    }
}

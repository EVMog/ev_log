﻿using EV_Log_Proto01_Trace.Model;
using EV_Log_Proto01_Trace.Model.Client;
using EV_Log_Proto01_Trace.Storages.AppInsights;
using EV_Log_Proto01_Trace.Storages.Firebase;
using System.Threading.Tasks;


namespace EV_Log_Proto01_Trace.Storages
{
    public static class Storage
    {
        public static FirebaseDB Firebase = new FirebaseDB();
        //public static ApplicationInsights ApplicationInsights = new ApplicationInsights();
     

        //public static SqlDB Sql
        //{
        //    get { return new SqlDB(); }
        //}

        public static async Task<bool> PostAsync(EventBase data, string type)
        {
            switch(type)
            {
                case StorageTypes.Firebase:
                    return await Firebase.PostAsync(data);
                //case StorageTypes.AppInsights:
                //    return ApplicationInsights.Post(data);
                //case StorageTypes.Sql:
                //    return await Sql.PostAsync(data);
                default:
                    return false;
            }
        }

        public static bool TryConnection(string type)
        {
            switch (type)
            {
                case StorageTypes.Firebase:
                    return Firebase.TryConnection();
                case StorageTypes.AppInsights:
                    return true;
                    // TODO -- Application Insights connection posting method here
                //case StorageTypes.Sql:
                //    return Sql.TryConnection();
                default:
                    return false;
            }
        }
    }
}

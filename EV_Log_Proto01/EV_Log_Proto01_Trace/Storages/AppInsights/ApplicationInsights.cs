﻿using ElbekVejrup.BusinessCentral.Telemetry;
using ElbekVejrup.BusinessCentral.Telemetry.DataContracts;
using EV_Log_Proto01_Trace.Helpers;
using EV_Log_Proto01_Trace.Model.Client;
using EV_Log_Proto01_Trace.Model.Events;
using System;
using System.Configuration;
using System.Reflection;

namespace EV_Log_Proto01_Trace.Storages.AppInsights
{
    public class ApplicationInsights
    {
        private BCtelemetryClient _bCTelemetryClient;
        private static string _instrumentationKey = ConfigurationManager.AppSettings["StorageInfo1"].ToString();
        private static string _tenant = ConfigurationManager.AppSettings["StorageInfo2"].ToString();
        private static string _clientVersionName = ConfigurationManager.AppSettings["ClientVersionName"].ToString();
        private static string _evLogVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        private static bool _doFlush = false;
        private bool _doLogging = true;

        public ApplicationInsights(bool doFlush =false, bool doLogging = false, bool developerMode = false)
        {
            _bCTelemetryClient = new BCtelemetryClient(
                _instrumentationKey,
                _tenant,
                _clientVersionName,
                _evLogVersion
                );
            _doFlush = doFlush;
            _doLogging = doLogging;
            _bCTelemetryClient.DeveloperMode = developerMode;
            Console.WriteLine("ApplicationInsights object created");
            Console.WriteLine($"instrumentation key was: {_instrumentationKey}");
        }

        public bool Post(EventBase data)
        {
            if (data is SQLLockedByUserEvent)
            {
                SQLLockedByUserEvent SqlData = (data as SQLLockedByUserEvent);
                _bCTelemetryClient.TrackSQLStatement(
                    data.ServerInstance,
                    data.ClientSessionId,
                    data.Tag,
                    data.Timestamp,
                    EventName.LockedByAnotherUser,
                    GetobjectType(),
                    SqlData.ObjectId,
                    long.Parse(SqlData.ExecutionTime),
                    SqlData.Message,
                    SqlData.SqlTableName
                    );
                Alert(data);
                if (_doFlush) { _bCTelemetryClient.Flush(); }
                return true;
            }
            //else if (data is SQLLongRunningEvent)
            //{
            //    SQLLongRunningEvent SqlData = (data as SQLLongRunningEvent);
            //    _bCTelemetryClient.TrackSQLStatement(
            //        data.ServerInstance,
            //        data.ClientSessionId,
            //        data.Tag,
            //        data.Timestamp,
            //        EventName.LongRunningSQLStatement,
            //        GetobjectType(),
            //        SqlData.ObjectId,
            //        long.Parse(SqlData.ExecutionTime),
            //        SqlData.Message,
            //        SqlData.SqlTableName
            //        );
            //    Alert(data);
            //    if (_doFlush) { _bCTelemetryClient.Flush(); }
            //    return true;

            //}
            else if (data is EventBaseControlId)
            {
                switch (data.ObjectType)
                {
                    case EventObjectTypes.ValidateField: return StandardTrackPerformanceWithControlId(EventName.ValidateField, ObjectType.Page);
                    case EventObjectTypes.Action: return StandardTrackPerformanceWithControlId(EventName.Action, ObjectType.Page);
                    default: 
                        return false;
                }
                bool StandardTrackPerformanceWithControlId(EventName eventName, ObjectType objectType)
                {
                    _bCTelemetryClient.TrackPerformance(
                            data.ServerInstance,
                            data.ClientSessionId,
                            data.Tag,
                            data.Timestamp,
                            eventName,
                            objectType,
                            (data as EventBasePlus).ObjectId,
                            long.Parse((data as EventBasePlus).ExecutionTime),
                            (data as EventBaseControlId).ControlId
                            );
                    Alert(data);
                    if (_doFlush) { _bCTelemetryClient.Flush(); }
                    return true;
                }
            }
            else if (data is EventBasePlus)
            {
                switch (data.ObjectType)
                {
                    case EventObjectTypes.Page: return StandardTrackPerformance(EventName.GetPage, ObjectType.Page);
                    case EventObjectTypes.CurrentRecord: return StandardTrackPerformance(EventName.AfterGetCurrRecord, ObjectType.Page);
                    case EventObjectTypes.Form: return TrackPerformanceForm();
                    case EventObjectTypes.Report: return StandardTrackPerformance(EventName.RunReport, ObjectType.Report);
                    default:
                        return false;
                }
                bool StandardTrackPerformance(EventName eventName, ObjectType objectType)
                {
                    _bCTelemetryClient.TrackPerformance(
                            data.ServerInstance,
                            data.ClientSessionId,
                            data.Tag,
                            data.Timestamp,
                            eventName,
                            objectType,
                            (data as EventBasePlus).ObjectId,
                            long.Parse((data as EventBasePlus).ExecutionTime));
                    Alert(data);
                    if (_doFlush) { _bCTelemetryClient.Flush(); }
                    return true;
                }
                bool TrackPerformanceForm()
                {
                    _bCTelemetryClient.TrackPerformance(
                        data.ServerInstance,
                        data.ClientSessionId,
                        data.Tag,
                        data.Timestamp,
                        EventName.OpenPage,
                        ObjectType.Page,
                        (data as FormEvent).ObjectId,
                        (data as FormEvent).SubObjectId,
                        long.Parse((data as FormEvent).ExecutionTime));
                    Alert(data);
                    if (_doFlush) { _bCTelemetryClient.Flush(); }
                    return true;
                }
            }
            else
            {
                if (data.ObjectType == EventObjectTypes.User)
                {
                    UserEvent userData = (data as UserEvent);
                    _bCTelemetryClient.TrackUserName(
                        data.ServerInstance,
                        data.ClientSessionId,
                        data.Tag,
                        data.Timestamp,
                        userData.User);
                    Alert(data);
                    if (_doFlush) { _bCTelemetryClient.Flush(); }
                    return true;
                }
                if (data.ObjectType == EventObjectTypes.Company)
                {
                    CompanyEvent companyData = (data as CompanyEvent);
                    _bCTelemetryClient.TrackCompanyName(
                        data.ServerInstance,
                        data.ClientSessionId,
                        data.Tag,
                        data.Timestamp,
                        companyData.CompanyId);
                    Alert(data);
                    if (_doFlush) { _bCTelemetryClient.Flush(); }
                    return true;
                }
                Console.WriteLine("-----------------");
                Console.WriteLine("FAILED TO POST!!");
                Console.WriteLine("-----------------");

                return false;
            }

            ObjectType GetobjectType()
            {
                if (data is SQLLongRunningEvent || data is SQLLockedByUserEvent)
                {
                    string type = data is SQLLockedByUserEvent ? (data as SQLLockedByUserEvent).SqlObjectType.ToLower() : (data as SQLLongRunningEvent).SqlObjectType.ToLower();

                    switch (type)
                    {
                        case "codeunit": return ObjectType.Codeunit;
                        case "page": return ObjectType.Page;
                        case "report": return ObjectType.Report;
                        case "table": return ObjectType.Table;
                        case "query": return ObjectType.Query;
                        case "xmlport": return ObjectType.XMLport;
                        default:
                            return ObjectType.None;
                    }
                }
                return ObjectType.None;
            }

        }

        private void Alert(EventBase eventBase)
        {
            if (_doLogging)
            {
                Console.WriteLine($"Application Insight posting {eventBase.ObjectType}");
            }
        }

        public bool TestConnection()
        {
            return true;
        }
    }
}

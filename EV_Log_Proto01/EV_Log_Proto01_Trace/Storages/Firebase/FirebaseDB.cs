﻿using EV_Log_Proto01_Trace.Model.Client;
using FireSharp.Config;
using FireSharp.Interfaces;
using System;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Storages.Firebase
{
    public class FirebaseDB
    {
        private readonly IFirebaseConfig config = new FirebaseConfig { AuthSecret = "YSPQWy6cDZIvar20pees3hBXFrXllyyZ4ekxOsNh", BasePath = "https://ev-log.firebaseio.com/" };
        private readonly IFirebaseClient client;

        public FirebaseDB()
        {
            client = new FireSharp.FirebaseClient(config);
        }

        public async Task<bool> PostAsync(EventBase data)
        {
            FireSharp.Response.PushResponse response;

            response = await client.PushTaskAsync($"{data.ServerInstance}/{data.ObjectType}", data);
            

            if (response != null)
            {
                return response.Result.Name != null ? true : false;
            }
            else
            {
                return false;
            }
        }


        public bool ClearCollection()
        {
            var response = client.Delete("events");
            return response.Success;
        }

        public bool TryConnection()
        {
            try
            {
                var tryPush = client.Push("", new { connectionStatus = "Testing" });
                var key = tryPush.Result.Name;
                var tryDelete = client.Delete("" + key);
                return tryDelete.Success;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public struct ClientVersions
    {
        public const string v14_037587_0 = "14.037587.0";
        public const string v11_0_31747_0 = "11.0.31747.0";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public struct DataCategories
    {
        public const string Page = "Page";
        public const string Report = "Report";
        public const string Company = "Company";
        public const string UserInfo = "UserInfo";
        public const string User = "User";
        public const string NST = "NST";
        public const string Management  = "Management";
        public const string Runtime = "Runtime";
        public const string Security = "Security";
        public const string Sql = "Sql";




    }
}

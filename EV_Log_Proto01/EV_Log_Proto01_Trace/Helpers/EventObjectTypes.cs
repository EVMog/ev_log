﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public struct EventObjectTypes
    {
        public const string Page = "Page";
        public const string Report = "Report";
        public const string Form = "Form";
        public const string User = "User";
        public const string Company = "Company";
        public const string CurrentRecord = "CurrentRecord";
        public const string ValidateField = "ValidateField";
        public const string SQL_LongRunning = "SQL_LongRunning";
        public const string SQL_LockedByUser = "SQL_LockedByUser";
        public const string Action = "Action";


    }
}

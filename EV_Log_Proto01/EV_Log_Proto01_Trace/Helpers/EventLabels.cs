﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public struct EventLabels
    {
        public const string ClosingScopeOpenForm = "Closing scope \"OpenForm";
        public const string ClosingScopeRunReport = "Closing scope \"RunReport";
        public const string UserAuthenticated = "User authenticated";
        public const string OpeningCompany = "Opening company";
        public const string AfterGetCurrRecord = "AfterGetCurrRecord";
        public const string ClosingAcopeValidateField = "Closing scope \"ValidateField";
        public const string NavCSideException = "NavCSideException";
        public const string ClosingScopeActionField = "ClosingScopeActionField";





    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Model
{
    public struct EventProviders
    {
        public const string Kernel = "Microsoft-Windows-Kernel-EventTracing";
        public const string Client = "Microsoft-DynamicsNAV-Common";
        public const string Server = "Microsoft-DynamicsNAV-Server";
        public const string Internal = "Microsoft-EtwDemo";
    }
}
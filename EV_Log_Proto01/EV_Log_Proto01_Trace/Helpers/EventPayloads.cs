﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public struct EventPayloads
    {
        public const string CompanyStart = "Opening company -- CompanyName:";
        public const string CompanyEnd = "ProcessId";

        public const string PageStart = "FormId:";
        public const string PageEnd = "ParentFormId";

        public const string PageStart2 = "PageId=";
        public const string PageEnd2 = "ProcessId";

        public const string ReportStart = "Closing scope \"Running report processing for report ID";
        public const string ReportEnd = "ProcessId";

        public const string ReportStart2 = "Closing scope \"Server side RunReport for report ID";

        public const string ExecutionTimeStart = "Execution time = ";
        public const string ExecutionTimeEnd = ", Exclusive time";

        public const string UserInfoStart = "Message User authenticated. Details - User name: ";
        public const string UserInfoEnd = "; User ID";

        public const string NstStart = "Server instance: ";
        public const string NstEnd = "Category:";

        public const string ControlIdStart = "ControlId: ";
        public const string ControlIdEnd = "ControlEventId: ";

        public const string AfterGetCurrentRecordIdStart = "ObjectId: ";
        public const string AfterGetCurrentRecordIdEnd = "MethodName";

        public const string ValidateFieldIdStart = "FormId: ";
        public const string ValidateFieldIdEnd = "ParentFormId: ";


        public const string ClientSessionIdStart = "ClientSessionId: ";
        public const string ClientSessionIdEnd = "ClientActivityId: ";

        public const string SqlLongRunningAppObjectIdStart = "AppObjectId: ";
        public const string SqlLongRunningAppObjectIdEnd = "AL CallStack:";

        public const string LockedByUserMessageStart = "AL CallStack: ";
        public const string LockedByUserMessageEnd = "end:";

        public const string LongRunningMessageStart = "AL CallStack: ";
        public const string LongRunningMessageEnd = "end:";
        
        public const string SqlTableNameStart = "CURRENTCOMPANY$:";
        public const string SqlTableNameEnd = "\" SET:";

        public const string FormIdStart = "FormIds: ";
        public const string FormIdEnd = "ProcessId";

        public const string ActionIdStart = "FormId: ";
        public const string ActionIdEnd = "ParentFormId: ";

        public const string SQLGetObjectTypeStart = "AppObjectType: ";
        public const string SQLGetObjectTypeEnd = "AppObjectId";










    }
}

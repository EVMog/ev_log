﻿
namespace EV_Log_Proto01_Trace.Model
{
    public struct EventTags
    {
        public const string T000003Z = "000003Z";
        public const string T0000051 = "0000051";
        public const string T00000SL = "00000SL";
        public const string T0000020 = "0000020";
        public const string T000007L = "000007L";

    }
}

﻿using EV_Log_Proto01_Trace.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV_Log_Proto01_Trace.Helpers
{
    public static class FilterLists
    {
        public static readonly List<string> pageClose = new List<string> { EventTags.T000003Z, "Closing scope", "page"};
        public static readonly List<string> reportClose = new List<string> { EventTags.T0000051, "Closing scope" };
        public static readonly List<string> companyOpen = new List<string> { "Opening company -- CompanyName:" };
        public static readonly List<string> companyOpen2 = new List<string> { "00000KV" };
        public static readonly List<string> NstFilter = new List<string> { "Server instance" };
        public static readonly List<string> CurrentRecordFilter = new List<string> { EventTags.T000003Z, "InvokeApplicationMethod", "AfterGetCurrRecord" };
        public static readonly List<string> SQLLongRunningFilter = new List<string> { EventTags.T000007L, "Long running SQL statement" };
        public static readonly List<string> SQLLockedByUserFilter = new List<string> { EventTags.T000007L, "Long running SQL statement" };
        public static readonly List<string> ValidateFieldFilter = new List<string> { EventTags.T000003Z, "Closing scope \"ValidateField" };
        public static readonly List<string> user = new List<string> { EventTags.T0000020 };
        public static readonly List<string> Action = new List<string> { "Closing scope \"ActionField" };



        //TODO Check if these are depricated
        public static readonly List<string> pageFilter02 = new List<string> { "00000Z6" };
        public static readonly List<string> reportOpen = new List<string> { "Starting scope \"Running report processing for report ID" };
        public static readonly List<string> reportClose2 = new List<string> { "Closing", "000000K" };

    }
}

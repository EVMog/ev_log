﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EV_Log_API.Controllers
{
    public class ServerController : ApiController
    {
        private readonly string connectionString =
            "Data Source=NSTEventListene\\BCDEMO;" +
            "Initial Catalog=EV_Log_TestDB;" +
            "Integrated Security=True;" +
            "Connect Timeout=30;" +
            "Encrypt=False;" +
            "TrustServerCertificate=False;" +
            "ApplicationIntent=ReadWrite;" +
            "MultiSubnetFailover=False";

        [HttpPost]
        public string Post([FromBody]Input obj)
        {
            Server lookUp = new Server();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                using (SqlCommand command = new SqlCommand("SELECT * FROM Clients WHERE Server = @Server", con))
                {
                    command.Parameters.AddWithValue("@Server", obj.Server);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            lookUp.Source = Convert.ToString(reader["Source"]);
                            lookUp.Password = Convert.ToString(reader["Password"]);
                        }
                    }
                }

                con.Close();
            }

            if (lookUp.Password == obj.Password)
            {
                Response response = new Response();
                response.Source = lookUp.Source;

                return JsonConvert.SerializeObject(response);
            }
            else
            {
                return JsonConvert.SerializeObject(new Response());
            }
        }

        public class Input
        {
            public string Server { get; set; }
            public string Password { get; set; }
        }

        private class Server
        {
            public string Source { get; set; }
            public string Password { get; set; }
        }

        private class Response
        {
            public string Source { get; set; }
        }
    }
}

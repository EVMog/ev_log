﻿using FireSharp.Config;
using FireSharp.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace EV_Log_API.Controllers
{
    public class StorageController : ApiController
    {
        private readonly string connectionString =
            "Data Source=NSTEventListene\\BCDEMO;" +
            "Initial Catalog=EV_Log_TestDB;" +
            "Integrated Security=True;" +
            "Connect Timeout=30;" +
            "Encrypt=False;" +
            "TrustServerCertificate=False;" +
            "ApplicationIntent=ReadWrite;" +
            "MultiSubnetFailover=False";

        private static readonly IFirebaseConfig config = new FirebaseConfig { AuthSecret = "YSPQWy6cDZIvar20pees3hBXFrXllyyZ4ekxOsNh", BasePath = "https://ev-log.firebaseio.com/" };
        private static readonly IFirebaseClient client = new FireSharp.FirebaseClient(config);

        [HttpGet]
        public string Get()
        {
            return "Get works";
        }

        [HttpPost]
        public List<string> Post([FromBody]Input data)
        {
            switch (data.Source)
            {
                case "Firebase":
                    return Firebase(data);
                case "Sql":
                    return Sql(data);
                default:
                    return null;
            }
        }

        private List<string> Firebase(Input data)
        {
            var path = data.Server;
            if (data.Type != "") { path = data.Server + "/" + data.Type; }
            if (data.User != "") { path = data.Server + "/" + data.Type + "/" + data.User; }

            var response = client.Get(path);

            var body = response.Body;
            body.Insert(0, "[");
            body.Insert(body.Length, "]");

            List<string> temp = JsonConvert.DeserializeObject<List<string>>(body);

            return temp;
        }

        private List<string> Sql(Input data)
        {
            var table = "";

            if (data.Type == "pages") { table = "TracePages"; }
            if (data.Type == "reports") { table = "TraceReports"; }

            var path = "SELECT UserID FROM " + table + " WHERE NST = @Server GROUP BY UserID FOR JSON AUTO";

            if (data.User != "") { path = "SELECT * FROM " + table + " WHERE NST = @Server AND UserID = @User FOR JSON AUTO"; }

            var jsonString = "";

            using (SqlConnection c = new SqlConnection(connectionString))
            {
                using (SqlCommand query = new SqlCommand(path, c))
                {
                    query.Parameters.AddWithValue("@Server", data.Server);
                    query.Parameters.AddWithValue("@Type", data.Type);
                    query.Parameters.AddWithValue("@User", data.User);

                    c.Open();

                    using (SqlDataReader reader = query.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            jsonString += reader.GetValue(0).ToString();
                        }
                    }
                    
                    c.Close();
                }
            }
            
            return JsonConvert.DeserializeObject<List<string>>(jsonString);
        }

        public class Input
        {
            public string Server { get; set; }
            public string Type { get; set; }
            public string User { get; set; }
            public string Source { get; set; }
        }
    }
}

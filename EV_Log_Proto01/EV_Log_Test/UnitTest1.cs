using Microsoft.VisualStudio.TestTools.UnitTesting;
using EV_Log_Proto01_Trace.Model.Client;
using EV_Log_Proto01_Trace.Model;
using System;
using EV_Log_Proto01_Trace.Helpers;

namespace EV_Log_Test
{
    [TestClass]
    public class UnitTest1
    {
        //private ClientPageEvent expectedObj =
        //    new ClientPageEvent(DateTime.Now, DataTypes.Page, "CRONUS DANMARK", "MICHAEL", "NSTEVENTLISTENE", "9060");

        //Tracer tracer = Tracer.Instance;

        //private string EventString = "Server instance: BC140\n" +
        //"Category: Management\n" +
        //"ClientSessionId: 3e141809-6b09-46bd-bec8-6c4ebe1e2d84\n" +
        //"ClientActivityId: 930bc1ad-a2f9-47d0-a844-36d88d5c85d4\n" +
        //"ServerSessionUniqueId: 12ed6326-26f5-4786-8560-90c1d6d5f8b2\n" +
        //"ServerActivityId: e0763e1d-4d8e-4887-a99a-25192e77cf95\n" +
        //"EventTime: 14:37:51.788671 (14,498.295 MSec)\n" +
        //"Message Starting scope \"GetPage\n" +
        //"TableId: 9053\n" +
        //"FormId: 9060\n" +
        //"ParentFormId: 0\n" +
        //"\".\n" +
        //"Message User authenticated. Details - User name: NSTEVENTLISTENE\\MICHAEL; User ID: d3b9843b-5b70-4a36-9061-67a7858b168c;\n" +
        //"Message Opening company -- CompanyName:CRONUS Danmark\n" +
        //"ProcessId: 8,704\n" +
        //"Tag: 0000020\n" +
        //"ThreadId: 591\n" +
        //"CounterInformation:\n";

        //[TestMethod]
        //public void TestTimestamp()
        //{
        //    var result = tracer.TraceTest(EWTstring);
        //    Assert.AreEqual(expectedObj.Timestamp, result.Timestamp);
        //}

        //[TestMethod]
        //public void TestType()
        //{
        //    var result = tracer.TraceTest(EventString);
        //    Assert.AreEqual(expectedObj.Type, result.Type);
        //}

        //[TestMethod]
        //public void TestCompany()
        //{
        //    var result = tracer.TraceTest(EventString);
        //    Assert.AreEqual(expectedObj.CompanyID, result.CompanyID);
        //}

        //[TestMethod]
        //public void TestUser()
        //{
        //    var result = tracer.TraceTest(EventString);
        //    Assert.AreEqual(expectedObj.UserID, result.UserID);
        //}

        //[TestMethod]
        //public void TestNST()
        //{
        //    var result = tracer.TraceTest(EventString);
        //    Assert.AreEqual(expectedObj.NST, result.NST);
        //}

        //[TestMethod]
        //public void TestPageID()
        //{
        //    var result = (ClientPageEvent)tracer.TraceTest(EventString);
        //    Assert.AreEqual(expectedObj.PageID, result.PageID);
        //}

        string bigString = "14.056.551";
        string smallString = "14.056.550";
        string shortString = "13.042.251";
        string longString = "13.042.251.765";
        string wrongString = "gfs.�'�lm.ksdf";

        [TestMethod]
        public void TestIsBiggerThanOrEqual1()
        {
            Assert.AreEqual(true,bigString.IsBiggerThanOrEqual(smallString));
        }

        [TestMethod]
        public void TestIsBiggerThanOrEqual2()
        {
            Assert.AreEqual(false, smallString.IsBiggerThanOrEqual(bigString));
        }

        [TestMethod]
        public void TestIsBiggerThanOrEqual3()
        {
            Assert.AreEqual(true, longString.IsBiggerThanOrEqual(shortString));
        }

        [TestMethod]
        public void TestIsBiggerThanOrEqual4()
        {
            Assert.AreEqual(false, shortString.IsBiggerThanOrEqual(longString));
        }

        [TestMethod]
        public void TestIsBiggerThanOrEqual5()
        {
            Assert.AreEqual(false, shortString.IsBiggerThanOrEqual(wrongString));
        }

        [TestMethod]
        public void TestIsBiggerThanOrEqual6()
        {
            Assert.AreEqual(false, wrongString.IsBiggerThanOrEqual(shortString));
        }

    }
}
